<?php

namespace Drupal\dh_viewsreference\Plugin\ViewsReferenceSetting;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\views\ViewExecutable;
use Drupal\viewsreference\Plugin\ViewsReferenceSettingInterface;

/**
 * The views reference setting title plugin.
 *
 * @ViewsReferenceSetting(
 *   id = "override_title",
 *   label = @Translation("Override title"),
 *   default_value = "",
 * )
 */
class ViewsReferenceOverrideTitle extends PluginBase implements ViewsReferenceSettingInterface {

    use StringTranslationTrait;

    /**
     * {@inheritdoc}
     */
    public function alterFormField(array &$form_field) {
        $form_field['#type'] = 'textfield';
        $form_field['#weight'] = 21;
    }

    /**
     * {@inheritdoc}
     */
    public function alterView(ViewExecutable $view, $value) {
        if (!empty($value)) {
            $view->display_handler->setOption('title', $value);
        }
    }

}
